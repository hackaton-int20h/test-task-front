import qs from 'query-string'

const API_HOST = 'http://109.206.162.110/api/v1'

class Api {
  constructor (baseURL) {
    this.baseURL = baseURL
  }

  getJson (response) {
    return response.then(res => res.json())
      .then(jsonRes => jsonRes.data)
  }

  getList ({ emotions, page, limit }) {
    // const headers = new Headers({ 'Access-Control-Allow-Origin': '*' })
    const params = qs.stringify({ page, limit, emotions: emotions || [] }, { arrayFormat: 'bracket' })

    return this.getJson(fetch(`${this.baseURL}/photos?${params}`, { method: 'get' }))
  }
}

export default new Api(API_HOST)
