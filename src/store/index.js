import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    display: 'lg'
  },
  mutations: {
    SET_DISPLAY_TYPE (state, payload) {
      state.display = payload
    }
  },
  actions: {
    windowResize ({ commit }) {
      const wsize = window.innerWidth
      let type = 'lg'

      if (wsize >= 900) {
        type = 'lg'
      } else if (wsize >= 600) {
        type = 'sm'
      } else {
        type = 'xs'
      }

      commit('SET_DISPLAY_TYPE', type)
    }
  }
})
